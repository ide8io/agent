package serial

import (
	"encoding/json"
	"log"
	"strconv"
	"strings"
	"sync"
	"time"

	"bitbucket.org/ide8io/go-serial"
	"bitbucket.org/ide8io/agent/common"
	"github.com/gorilla/websocket"
)

type SerialTunnel struct {
	target    string
	ws        *websocket.Conn
	kill      chan struct{}
	terminate bool
}

func (t *SerialTunnel) SetTarget(target string) {
	if t.target == target {
		log.Println("Tunnel already running")
		return
	}
	t.target = target

	if t.ws != nil {
		t.ws.Close()
	}
}

func (t *SerialTunnel) Close() {
	t.terminate = true
	select {
	case t.kill <- struct{}{}:
	default:
	}

	if t.ws != nil {
		t.ws.Close()
	}
}

type serialTunnelMessage struct {
	Type    string `json:"type"`
	Id      string `json:"id"`
	Payload string `json:"payload"`
}

func trimQuotedString(str string) string {
	return strings.TrimSuffix(strings.TrimPrefix(str, "\""), "\"")
}

func (t *SerialTunnel) tunnel(target string, serhandler *serialHandler) {
	for !t.terminate {
		startTime := time.Now()
		var err error
		log.Println("opening tunnel to", target, "...")
		t.ws, _, err = websocket.DefaultDialer.Dial(target, nil)
		if err != nil {
			log.Println("tunnel:", err)
			duration := time.Now().Sub(startTime)
			if duration.Seconds() < 10 {
				select {
				case <-t.kill:
					return
				case <-time.After(time.Second * 10):
				}
			}
			continue
		}
		log.Println("tunnel openened")

		writemu := sync.Mutex{}
		write := func(messageType int, data []byte) error {
			writemu.Lock()
			defer writemu.Unlock()
			t.ws.SetWriteDeadline(time.Now().Add(common.WriteWait))
			return t.ws.WriteMessage(messageType, data)
		}

		killpong := make(chan struct{})
		go func() {
			for {
				select {
				case <-time.After(common.PongPeriod):
					err := write(websocket.PingMessage, []byte{})
					if err != nil {
						log.Println("tunnel ping write:", err)
					}
				case <-killpong:
					return
				}
			}
		}()

		kill := make(chan struct{})
		status := make(chan string)

		go func() {
		loop:
			for {
				select {
				case data := <-serhandler.output:
					msg := serialTunnelMessage{Type: "uart:output", Payload: trimQuotedString(strconv.Quote(string(data)))}
					jb, err := json.Marshal(msg)
					if err != nil {
						log.Println("tunnel:", err)
					}
					//log.Println("output:", string(jb))
					err = write(websocket.TextMessage, jb)
					if err != nil {
						log.Println("tunnel WriteMessage:", err)
						break loop
					}
				case data := <-serhandler.status:
					msg := serialTunnelMessage{Type: "uart:status", Payload: data}
					jb, err := json.Marshal(msg)
					if err != nil {
						log.Println("tunnel:", err)
					}
					//log.Println("output:", string(jb))
					err = write(websocket.TextMessage, jb)
					if err != nil {
						log.Println("tunnel WriteMessage:", err)
						break loop
					}
				case data := <-status:
					msg := serialTunnelMessage{Type: "uart:status", Payload: data}
					jb, err := json.Marshal(msg)
					if err != nil {
						log.Println("tunnel:", err)
					}
					//log.Println("output:", string(jb))
					err = write(websocket.TextMessage, jb)
					if err != nil {
						log.Println("tunnel WriteMessage:", err)
						break loop
					}
				case <-kill:
					return
				}
			}
			t.ws.Close()
			<-kill
		}()

		t.ws.SetReadDeadline(time.Now().Add(common.PongWait))
		t.ws.SetPongHandler(func(string) error {
			t.ws.SetReadDeadline(time.Now().Add(common.PongWait))
			return nil
		})

		for {
			_, b, err := t.ws.ReadMessage()
			if err != nil {
				log.Println("tunnel ReadMessage:", err)
				break
			}
			//log.Println("input:", string(b))
			var msg serialTunnelMessage
			err = json.Unmarshal(b, &msg)
			if err != nil {
				log.Println("tunnel:", err)
				log.Println("tunnel bad input json:", string(b))
				continue
			}
			payload, err := strconv.Unquote("\"" + msg.Payload + "\"")
			if err != nil {
				log.Println("tunnel Unquote:", err)
				continue
			}
			if msg.Type == "uart:input" {
				serhandler.input <- []byte(payload)
			} else if msg.Type == "uart:dtr" {
				serhandler.control <- serialControl{"DTR", payload}
			} else if msg.Type == "uart:baud" {
				serhandler.control <- serialControl{"baud", payload}
			} else if msg.Type == "uart:arduino-bootloader" {
				serhandler.control <- serialControl{"DTR", "0"}
				serhandler.control <- serialControl{"RTS", "0"}
				time.Sleep(time.Second / 20)
				serhandler.control <- serialControl{"DTR", "1"}
				serhandler.control <- serialControl{"RTS", "1"}
				time.Sleep(time.Second / 20)
				status <- "ready"
			} else {
				log.Println("Unknown message type:", msg.Type)
			}
		}

		killpong <- struct{}{}
		kill <- struct{}{}
		log.Println("tunnel closed")
		t.ws.Close()

		duration := time.Now().Sub(startTime)
		if duration.Seconds() < 10 {
			time.Sleep(10*time.Second - duration)
		}
	}
	serhandler.Close()
}

func OpenSerialTunnel(port string, baud int, target string) *SerialTunnel {
	serh := openSerialHandler(port, baud)
	tun := &SerialTunnel{target: target, kill: make(chan struct{})}
	go tun.tunnel(target, serh)
	return tun
}

type serialControl struct {
	key string
	val string
}

type serialHandler struct {
	ok      bool
	input   chan []byte
	output  chan []byte
	control chan serialControl
	status  chan string
	port    serial.Port
}

func openSerialHandler(serialPort string, baud int) *serialHandler {
	h := &serialHandler{
		ok:      true,
		input:   make(chan []byte),
		output:  make(chan []byte),
		control: make(chan serialControl),
		status:  make(chan string),
	}

	go func() {
		for h.ok {
			startTime := time.Now()
			mode := serial.Mode{
				BaudRate: baud,
			}
			port, err := serial.Open(serialPort, &mode)
			if err != nil {
				log.Printf("Failed to open serial port %s: %v", serialPort, err)
				time.Sleep(10*time.Second - time.Now().Sub(startTime))
				continue
			}
			log.Println("serial port opened:", serialPort)
			h.port = port

			kill := make(chan struct{})

			go func() {
				for {
					select {
					case data := <-h.input:
						//log.Printf("write(%d): %s", len(data), strconv.Quote(string(data)))
						_, err := port.Write(data)
						if err != nil {
							log.Println("serial write:", err)
						}
					case ctrl := <-h.control:
						//log.Println("ctrl:", ctrl.key, ctrl.val)
						if ctrl.key == "DTR" {
							if ctrl.val == "1" {
								port.SetDTR(true)
							} else if ctrl.val == "0" {
								port.SetDTR(false)
							} else {
								log.Println("serial control: unknown val", ctrl.val, "for", ctrl.key)
							}
						} else if ctrl.key == "RTS" {
							if ctrl.val == "1" {
								port.SetRTS(true)
							} else if ctrl.val == "0" {
								port.SetRTS(false)
							} else {
								log.Println("serial control: unknown val", ctrl.val, "for", ctrl.key)
							}
						} else if ctrl.key == "baud" {
							baud, err := strconv.Atoi(ctrl.val)
							if err != nil {
								log.Println("serial control: bad baudrate", ctrl.val)
							} else {
								mode.BaudRate = baud
								port.SetMode(&mode)
							}
						} else {
							log.Println("serial control: unknown control", ctrl.key)
						}
					case <-kill:
						return
					}
				}
			}()

			stop := make(chan struct{})
			go func() {
				select {
				case h.status <- "online":
					select {
					case <-stop:
						select {
						case h.status <- "offline":
						case <-time.After(time.Second):
						}
					}
				case <-stop:
					// No need to deliver any offline since online have never been delivered
				}
			}()

			b := make([]byte, 128)
			for h.ok {
				n, err := port.Read(b)
				if err != nil {
					log.Println("serial read:", err)
					break
				}
				if n == 0 {
					// Seems like we get 0 chars received when device is unplugged on linux
					log.Println("serial read: No data read")
					break
				}
				//log.Printf("read(%d): %s", n, strconv.Quote(string(b[:n])))
				// Give a new copy thru channel since we reuse our buffer
				data := make([]byte, n)
				copy(data, b[:n])
				h.output <- data
			}

			kill <- struct{}{}
			stop <- struct{}{}
			port.Close()
			log.Println("serial port closed:", serialPort)
		}
	}()

	return h
}

func (h *serialHandler) Close() {
	log.Println("closing serial port...")
	h.ok = false
	port := h.port
	if port != nil {
		port.Close()
	}
}
