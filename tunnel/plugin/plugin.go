package tunnel_plugin

import (
	"context"
	"crypto/tls"
	"log"
	"net/url"
	"strings"
	"time"

	"bitbucket.org/ide8io/ide8.io/backend/api/tunnel"
	"bitbucket.org/ide8io/agent/plugin"
	"bitbucket.org/ide8io/agent/util"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/metadata"
)

type Tunnel struct {
	plugin plugin.Plugin
	target string
	cancel context.CancelFunc
}

func New(plugin plugin.Plugin, target string) *Tunnel {
	t := &Tunnel{plugin: plugin}
	t.SetTarget(target)
	return t
}

func (t *Tunnel) SetTarget(target string) {
	if target == t.target {
		log.Println("Tunnel already running")
		return
	}
	t.target = target
	if t.cancel != nil {
		t.cancel()
		t.cancel = nil
	}
	var ctx context.Context
	ctx, t.cancel = context.WithCancel(context.Background())

	go t.connect(ctx, target)
}

func (t *Tunnel) Close() {
	t.cancel()
	t.plugin.Close()
}

func (t *Tunnel) connect(ctx context.Context, target string) {
	log.Println("target:", target)
	u, err := url.Parse(target)
	if err != nil {
		log.Println(err)
		return
	}
	log.Println("url:", u.Path)
	parr := strings.SplitN(u.Path, "/", 5)
	log.Println("parr:", parr)
	if len(parr) != 5 {
		log.Println("Invalid tunnel path")
		return
	}
	var dopts []grpc.DialOption
	if u.Scheme == "wss" || u.Scheme == "https" {
		dopts = []grpc.DialOption{grpc.WithTransportCredentials(credentials.NewTLS(&tls.Config{}))}
	} else {
		dopts = []grpc.DialOption{grpc.WithInsecure()}
	}
	md := metadata.MD{
		"workspace": {parr[2] + "/" + parr[3]},
		"tunnelid":  {parr[4]},
		"token":     {"token"},
	}
	tctx := metadata.NewOutgoingContext(ctx, md)

	startTime := time.Time{}
	for {
		err := util.RelaxReconnect(ctx, startTime, 10*time.Second)
		if err == context.Canceled {
			break
		}
		startTime = time.Now()
		conn, err := grpc.DialContext(ctx, u.Hostname()+":7070", dopts...)
		if err != nil {
			log.Println(util.GetGRPCErrorString(err))
			continue
		}
		client := api_tunnel.NewTunnelApiClient(conn)
		tunnel, err := client.Tunnel(tctx)
		if err != nil {
			conn.Close()
			log.Println("tunnel client:", util.GetGRPCErrorString(err))
			continue
		}

		go func() {
			for {
				msg, err := t.plugin.Recv(ctx)
				if err != nil {
					log.Println("plugin recv:", err)
					break
				}
				err = tunnel.Send(msg)
				if err != nil {
					log.Println("tunnel send:", util.GetGRPCErrorString(err))
					break
				}
			}
			tunnel.CloseSend()
		}()

		t.plugin.Online()

		for {
			msg, err := tunnel.Recv()
			if err != nil {
				log.Println("tunnel recv:", util.GetGRPCErrorString(err))
				break
			}
			err = t.plugin.Send(ctx, msg)
			if err != nil {
				log.Println("plugin send:", err)
				break
			}
		}
		conn.Close()
	}
}
