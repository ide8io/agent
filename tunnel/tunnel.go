package tunnel

type Tunnel interface {
	SetTarget(target string)
	Close()
}
