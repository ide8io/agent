#!/bin/bash -ex

gitver=$(git describe --always --tags --dirty)

builddir=ide8agent-headless-${gitver}

mkdir ${builddir}

go build -o ${builddir}/ide8agent-headless -ldflags "-s -w -X main.gitver=$gitver" headless/ide8agent.go

if [ ! -z "$PLUGINS_IMPORT" ]; then
    mkdir $builddir/plugins
    cd $builddir/plugins
    for plugin in $PLUGINS_IMPORT/*.tar.bz2; do
        [ -e "$plugin" ] || continue
        tar xjf $plugin
    done
    cd -
fi

ftype=$(file ${builddir}/ide8agent-headless)
if [[ $ftype == *"80386"* ]]; then
    arch="i386"
elif [[ $ftype == *"x86-64"* ]]; then
    arch="x86_64"
elif [[ $ftype == *"ARM"* ]]; then
    aver=$(readelf -a -W ${builddir}/ide8agent-headless | grep Tag_CPU_arch | cut -d " " -f 4)
    arch=arm$aver
else
    arch=$(uname -m)
fi
archive=$builddir-linux-$arch.tar.gz
tar czf $archive $builddir

echo "Created archive $archive"

rm -rf $builddir
