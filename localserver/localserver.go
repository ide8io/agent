package localserver

import (
	"crypto/rand"
	"encoding/base64"
	"html/template"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"strconv"
	"strings"
)

type Server struct {
	Addr       string
	Port       int
	UserCh     chan string
	Token      string
	domain     string
	httpServer http.Server
}

func generateRandom(n int) ([]byte, error) {
	b := make([]byte, n)
	_, err := rand.Read(b)
	return b, err
}

func generateRandomString(n int) (string, error) {
	b, err := generateRandom(n)
	return base64.URLEncoding.EncodeToString(b), err
}

func New(domain string) (*Server, error) {
	port := 8361
	addr := ""
	var listener net.Listener
	for i := 0; i < 10; i++ {
		// Try range of 10 ports to try to end up in predictable range
		addr = "localhost:" + strconv.Itoa(port+i)
		var err error
		listener, err = net.Listen("tcp", addr)
		if err == nil {
			break
		}
	}
	if listener == nil {
		// Try random port as last resort
		addr = "localhost:"
		var err error
		listener, err = net.Listen("tcp", addr)
		if err != nil {
			return nil, err
		}
		addr += strconv.Itoa(listener.Addr().(*net.TCPAddr).Port)
	}
	log.Println("addr:", addr)
	token, err := generateRandomString(16)
	if err != nil {
		return nil, err
	}
	token = strings.TrimRight(token, "=")
	srv := &Server{Addr: "http://" + addr, Port: port, UserCh: make(chan string), Token: token, domain: domain}

	http.HandleFunc("/user", srv.handleUser)

	go func() {
		if err := srv.httpServer.Serve(listener); err != nil {
			log.Printf("localserver: http.Serve() error: %s", err)
		}
	}()
	return srv, nil
}

func (s *Server) Shutdown() {
	s.httpServer.Shutdown(nil)
}

func (s *Server) handleUser(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", s.domain)
	w.Header().Set("Access-Control-Allow-Methods", "PUT POST")
	if r.Method == "OPTIONS" {
		return
	} else if r.Method == "PUT" {
		token := r.FormValue("token")
		if token != s.Token {
			http.Error(w, "Invalid token", http.StatusUnauthorized)
			return
		}
		defer r.Body.Close()
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			http.Error(w, "Bad request body: "+err.Error(), http.StatusBadRequest)
			return
		}
		s.UserCh <- string(body)
		return
	} else if r.Method == "POST" {
		token := r.FormValue("token")
		if token != s.Token {
			http.Error(w, "Invalid token", http.StatusUnauthorized)
			return
		}
		user := r.FormValue("user")
		s.UserCh <- user
		setuserTemplate.Execute(w, map[string]string{
			"url":  s.domain + "/users/" + user,
			"user": user,
		})
		return
	} else {
		http.Error(w, "Unsupported method: "+r.Method, http.StatusMethodNotAllowed)
		return
	}
	http.Error(w, "Invalid request: "+r.Method+" "+r.URL.RawPath, http.StatusBadRequest)
}

const setuserTemplateHTML = `<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>IDE8 Agent</title>
</head>
<body>
IDE8 agent have been assigned to user {{ .user }}<br>
<br>
<a href="{{ .url }}">Go to your IDE8 user area</a>
</body>
</html>`

var setuserTemplate = template.Must(template.New("setuser").Parse(setuserTemplateHTML))
