package main

import (
	"flag"
	"log"
	"net/url"
	"os"
	"runtime"

	"bitbucket.org/ide8io/ide8.io/backend/gizmo"
	"bitbucket.org/ide8io/agent/connect"
	"bitbucket.org/ide8io/agent/localserver"
	"bitbucket.org/ide8io/agent/plugin"
	"bitbucket.org/ide8io/agent/resource/scanner"
	"bitbucket.org/ide8io/agent/userconfig"
	"bitbucket.org/ide8io/agent/util"
	"github.com/skratchdot/open-golang/open"
)

var gitver string

func main() {
	log.SetFlags(log.LstdFlags | log.Lmicroseconds)
	if gitver == "" {
		log.Println(os.Args[0], "starting")
	} else {
		log.Println(os.Args[0], "version", gitver, "starting")
	}
	staging := flag.Bool("staging", false, "Connect to staging server")
	serverflag := flag.String("server", "wss://ide8.io/_connect", "Server URL")
	resourceflag := flag.String("resource", "", "Resource mapping between device and type.\n\tE.g.: /dev/ttyUSB0=ArduinoUNO\n\tMultiple mappings can be given with , as separator.")
	pluginflag := flag.String("plugin", "", "Plugin mapping between name and binary")
	username := flag.String("user", "", "IDE8 user to assign to")
	public := flag.Bool("public", false, "Make resource public")
	flag.Parse()

	var server string
	if *staging {
		server = "wss://staging.ide8.io/_connect"
	} else {
		server = *serverflag
	}

	serverinfo, err := util.GetServerInfo(server)
	if err != nil {
		log.Fatal(err)
	}

	conf, err := userconfig.GetConfig()
	if err != nil {
		log.Fatalf("Failed to get config: %s", err)
	}

	if *public {
		empty := ""
		username = &empty
	} else if *username == "" {
		username = &conf.Username
	}

	if *username == "" {
		log.Println("Connecting with public access")
	} else {
		log.Println("Connecting as user", *username)
	}

	resTypeMapping, err := util.ParseMultiMapping(*resourceflag)
	if err != nil {
		log.Fatalln("Failed to parse resource flag:", err)
	}

	pluginMapping, err := util.ParseMultiMapping(*pluginflag)
	if err != nil {
		log.Fatalln("Failed to parse plugin flag:", err)
	}
	plugins, err := plugin.Scan(pluginMapping)
	if err != nil {
		log.Fatalln("Plugin scan:", err)
	}

	// Open local web server
	ls, err := localserver.New(serverinfo.GetHttpBase().String())
	if err != nil {
		log.Fatalf("localserver: %s", err)
	}

	go func() {
		ress := scanner.Init(resTypeMapping)
		rds := []gizmo.NamedGizmo{}
		for _, r := range ress {
			log.Println("Found resource: ", r)
			rds = append(rds, r.Gizmo())
		}
		// Get any stored agent id and session for server
		id, session, err := conf.GetSession(server)
		if err != nil {
			log.Fatalln("Failed to get or create session:", err)
		}
		conn := connect.New(plugins.Plugins, rds, server, id, session, *username, gitver)

		connected := false
		for {
			select {
			case connected = <-conn.ConnCh:
				log.Println("connected:", connected)
				SetSystrayUser(*public, *username, connected)
			case msg := <-conn.UpgradeCh:
				upgrURL := serverinfo.GetHttpBase().String() + "/download/agent?updatefrom=" + gitver + "&os=" + runtime.GOOS + "&arch=" + runtime.GOARCH
				open.Run(upgrURL)
				log.Fatalf("IDE8 agent version is too old! Please upgrade!\n" + msg + "\n" + upgrURL)
			case tun := <-conn.TunnelCh:
				log.Println("tunnel:", tun)
				res := scanner.GetResource(tun.ResourceId)
				if res == nil {
					continue
				}
				if tun.Target == "" {
					res.CloseTunnel()
					continue
				}
				targeturl, err := url.Parse(tun.Target)
				if err != nil {
					log.Println("TunnelCh:", err)
					continue
				}
				if !targeturl.IsAbs() {
					targeturl.Scheme = serverinfo.URL.Scheme
					targeturl.Host = serverinfo.URL.Host
				}
				res.SetTunnel(plugins.CreatePlugin, targeturl.String())
			case nsess := <-conn.SetSessCh:
				err := conf.SetSession(server, nsess.Name, nsess.Secret)
				if err != nil {
					log.Println("Error storing session:", err)
				}
			case newusername := <-ls.UserCh:
				log.Println("Setting new user: ", newusername)
				conf.Username = newusername
				err := conf.Store()
				if err != nil {
					log.Println("Error storing session:", err)
				}
				username = &conf.Username
				conn.SetUser(*username)
				SetSystrayUser(*public, *username, connected)
			case res := <-scanner.AddCh:
				log.Println("add:", res)
				conn.AddGizmo(res.Gizmo())
			case res := <-scanner.RemCh:
				log.Println("rem:", res)
				conn.RemoveGizmo(res.Gizmo().ID)
			}
		}
	}()

	SetSystrayUser(*public, *username, false)
	RunSystray(serverinfo, ls.Port, ls.Token)
}
