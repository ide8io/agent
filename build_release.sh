#!/bin/bash -ex

gitver=$(git describe --always --tags --dirty)

builddir=ide8agent-${gitver}

mkdir ${builddir}

go build -ldflags "-s -w -X main.gitver=$gitver" -o ${builddir}/ide8agent

if [ ! -z "$PLUGINS_IMPORT" ]; then
    mkdir $builddir/plugins
    cd $builddir/plugins
    for plugin in $PLUGINS_IMPORT/*.zip; do
        [ -e "$plugin" ] || continue
        unzip $plugin
    done
    for plugin in $PLUGINS_IMPORT/*.tar.bz2; do
        [ -e "$plugin" ] || continue
        tar xjf $plugin
    done
    cd -
fi

archive=""
if [ "$(uname)" == "Darwin" ]; then
    archive=$builddir-macos.zip
    zip -r $archive $builddir
elif [ "$(expr substr $(uname -s) 1 5)" == "Linux" ]; then
    ftype=$(file ${builddir}/ide8agent)
    if [[ $ftype == *"80386"* ]]; then
        arch="i386"
    elif [[ $ftype == *"x86-64"* ]]; then
        arch="x86_64"
    elif [[ $ftype == *"ARM"* ]]; then
        aver=$(readelf -a -W ${builddir}/ide8agent | grep Tag_CPU_arch | cut -d " " -f 4)
        arch=arm$aver
    else
        arch=$(uname -m)
    fi
    archive=$builddir-linux-$arch.tar.gz
    tar czf $archive $builddir
elif [ "$(expr substr $(uname -s) 1 10)" == "MINGW32_NT" ]; then
    archive=$builddir-win32.zip
    zip -r $archive $builddir
elif [ "$(expr substr $(uname -s) 1 10)" == "MINGW64_NT" ]; then
    archive=$builddir-win64.zip
    zip -r $archive $builddir
elif [ "$(expr substr $(uname -s) 1 7)" == "MSYS_NT" ]; then
    ftype=$(file ${builddir}/ide8agent.exe)
    if [[ $ftype == *"80386"* ]]; then
        arch="i386"
        archive=$builddir-win32.zip
    elif [[ $ftype == *"x86-64"* ]]; then
        arch="x86_64"
        archive=$builddir-win64.zip
    else
        arch=$(uname -m)
        archive=$builddir-windows-$arch.zip
    fi
    zip -r $archive $builddir
else
    echo "Unsupported system $(uname -s)"
    exit 1
fi

echo "Created archive $archive"

rm -rf $builddir
