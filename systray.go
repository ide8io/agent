package main

import (
	"log"
	"net/url"
	"os"
	"runtime"
	"strconv"
	"time"

	"bitbucket.org/ide8io/agent/icon"
	"bitbucket.org/ide8io/agent/util"
	"github.com/getlantern/systray"
	"github.com/skratchdot/open-golang/open"
)

var siteURL string
var assignURL string
var mUser *systray.MenuItem
var title string
var tooltip string
var dest string
var assigned bool

func RunSystray(serverinfo *util.ServerInfo, localport int, token string) {
	if runtime.GOOS == "linux" {
		if os.Getenv("DISPLAY") == "" {
			// Running without gui
			select {}
		}
	}

	u := serverinfo.GetHttp()
	u.Path += "/assign"
	q := url.Values{}
	q.Set("port", strconv.Itoa(localport))
	q.Set("token", token)
	u.RawQuery = q.Encode()
	assignURL = u.String()
	u.RawQuery = ""
	u.Path = ""
	siteURL = u.String()

	onReady := func() {
		systray.SetIcon(icon.Data)
		systray.SetTitle("IDE8")
		systray.SetTooltip("IDE8 Agent")
		mUser = systray.AddMenuItem(title, tooltip)
		mQuit := systray.AddMenuItem("Quit", "Quit the agent")
		go func() {
		loop:
			for {
				select {
				case <-mUser.ClickedCh:
					if dest != "" {
						open.Run(dest)
					}
				case <-mQuit.ClickedCh:
					systray.Quit()
					log.Println("quitting")
					break loop
				}
			}
			os.Exit(0)
		}()
	}

	onExit := func() {
		log.Println("onExit")
	}

	systray.Run(onReady, onExit)
}

func SetSystrayUser(public bool, username string, connected bool) {
	if connected {
		title = "Connected"
	} else {
		title = "Connecting"
	}
	title += " to IDE8 as "
	if public {
		title += "public agent"
		dest = siteURL
		tooltip = "Open IDE8 in browser"
	} else if username == "" {
		title = "Assign to IDE8 account ..."
		dest = assignURL
		tooltip = "Assign local running IDE8 agent to your IDE8 account"
		if dest != "" && !assigned {
			assigned = true
			go func() {
				time.Sleep(time.Second)
				open.Run(dest)
			}()
		}
	} else {
		title += username
		dest = siteURL + "/users/" + username
		tooltip = "Open IDE8 user account in browser"
	}

	if mUser != nil {
		mUser.SetTitle(title)
		mUser.SetTooltip(tooltip)
	}
}
