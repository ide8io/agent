package userconfig

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"os/user"
	"path/filepath"
	"strings"
)

type Config struct {
	Username string    `json:"username"`
	Sessions []Session `json:"sessions"`
}

type Session struct {
	Server string `json:"server"`
	Name   string `json:"name"`
	Secret string `json:"secret"`
}

func GetConfig() (Config, error) {
	u, err := user.Current()
	if err != nil {
		return Config{}, err
	}
	dotdir := filepath.Join(u.HomeDir, ".ide8agent")
	if _, err := os.Stat(dotdir); os.IsNotExist(err) {
		return Config{}, nil
	}

	conffile := filepath.Join(dotdir, "config.json")
	if _, err := os.Stat(conffile); os.IsNotExist(err) {
		return Config{}, nil
	}

	data, err := ioutil.ReadFile(conffile)
	if err != nil {
		return Config{}, err
	}

	conf := Config{}
	err = json.Unmarshal(data, &conf)
	if err != nil {
		return conf, err
	}

	return conf, nil
}

func (c *Config) Store() error {
	u, err := user.Current()
	if err != nil {
		return err
	}
	dotdir := filepath.Join(u.HomeDir, ".ide8agent")
	if _, err := os.Stat(dotdir); os.IsNotExist(err) {
		err := os.Mkdir(dotdir, 0700)
		if err != nil {
			return err
		}
	}

	b, err := json.MarshalIndent(c, "", "  ")
	if err != nil {
		return err
	}
	conffile := filepath.Join(dotdir, "config.json")
	err = ioutil.WriteFile(conffile, b, 0600)
	return err
}

func getSessionId() (string, error) {
	hostname, err := os.Hostname()
	if err != nil {
		return "", err
	}
	u, err := user.Current()
	if err != nil {
		return "", err
	}
	// Remove unwanted machine prefix on windows
	usplit := strings.Split(u.Username, "\\")
	hostusername := usplit[len(usplit)-1]
	id := hostusername + "@" + hostname
	id = strings.Replace(id, "/", "_", -1)
	return id, nil
}

func (c *Config) GetSession(server string) (string, string, error) {
	for i := range c.Sessions {
		if c.Sessions[i].Server == server {
			return c.Sessions[i].Name, c.Sessions[i].Secret, nil
		}
	}
	name, err := getSessionId()
	return name, "", err
}

func (c *Config) SetSession(server string, name string, secret string) error {
	for i := range c.Sessions {
		if c.Sessions[i].Server == server {
			c.Sessions[i].Name = name
			c.Sessions[i].Secret = secret
			return c.Store()
		}
	}
	c.Sessions = append(c.Sessions, Session{server, name, secret})
	return c.Store()
}
