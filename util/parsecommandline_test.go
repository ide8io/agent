package util_test

import (
	"testing"

	"bitbucket.org/ide8io/agent/util"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestParseCommandLineQuoted(t *testing.T) {
	args, err := util.ParseCommandLine(`"foo bar" bar "foo \"bar\""`)
	require.Nil(t, err)
	require.Equal(t, 3, len(args))
	assert.Equal(t, "foo bar", args[0])
	assert.Equal(t, "bar", args[1])
	assert.Equal(t, `foo "bar"`, args[2])
}
