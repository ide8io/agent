package util

import "net/url"

type ServerInfo struct {
	URL *url.URL
}

func GetServerInfo(server string) (*ServerInfo, error) {
	u, err := url.Parse(server)
	if err != nil {
		return nil, err
	}

	return &ServerInfo{URL: u}, nil
}

func (si *ServerInfo) getHttpScheme() string {
	if si.IsSecure() {
		return "https"
	} else {
		return "http"
	}
}

func (si *ServerInfo) GetHttp() *url.URL {
	u := *si.URL
	u.Scheme = si.getHttpScheme()
	return &u
}

func (si *ServerInfo) GetHttpBase() *url.URL {
	u := *si.URL
	u.Scheme = si.getHttpScheme()
	u.Path = ""
	return &u
}

func (si *ServerInfo) IsSecure() bool {
	return si.URL.Scheme == "wss" || si.URL.Scheme == "https"
}
