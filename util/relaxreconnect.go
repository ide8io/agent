package util

import (
	"context"
	"time"
)

func RelaxReconnect(ctx context.Context, startTime time.Time, delay time.Duration) error {
	duration := time.Now().Sub(startTime)
	if duration < delay {
		select {
		case <-time.After(delay - duration):
		case <-ctx.Done():
			return ctx.Err()
		}
	}
	return nil
}
