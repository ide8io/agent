package util

import "google.golang.org/grpc/status"

func GetGRPCErrorString(err error) string {
	serr, ok := status.FromError(err)
	if ok {
		return serr.Message()
	} else {
		return err.Error()
	}
}
