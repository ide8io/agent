package util

import (
	"fmt"
	"strings"
)

func ParseMultiMapping(arg string) (map[string]string, error) {
	mapping := map[string]string{}
	list := strings.Split(arg, ",")
	for _, rm := range list {
		if len(rm) == 0 {
			continue
		}
		rmsplit := strings.SplitN(rm, "=", 2)
		if len(rmsplit) != 2 {
			return nil, fmt.Errorf("missing equal (=) in arg: %v", rm)
		}
		mapping[rmsplit[0]] = rmsplit[1]
	}
	return mapping, nil
}
