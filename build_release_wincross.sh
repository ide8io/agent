#!/bin/bash -ex

gitver=$(git describe --always --tags --dirty)

builddir=ide8agent-${gitver}

mkdir ${builddir}

go build -ldflags "-s -w -X main.gitver=$gitver" -o ${builddir}/ide8agent.exe

if [ ! -z "$PLUGINS_IMPORT" ]; then
    mkdir $builddir/plugins
    cd $builddir/plugins
    for plugin in $PLUGINS_IMPORT/*.tar.bz2; do
        [ -e "$plugin" ] || continue
        tar xjf $plugin
    done
    cd -
fi

archive=""
ftype=$(file ${builddir}/ide8agent.exe)
if [[ $ftype == *"80386"* ]]; then
    arch="i386"
    archive=$builddir-win32.zip
elif [[ $ftype == *"x86-64"* ]]; then
    arch="x86_64"
    archive=$builddir-win64.zip
else
    arch=$(uname -m)
    archive=$builddir-windows-$arch.zip
fi

if [ ! -z "$CODESIGNCERT" ]; then
    cd $builddir
    osslsigncode sign -pkcs12 $CODESIGNCERT -pass $CODESIGNCERTPW -n ide8agent -i http://ide8.io -in ide8agent.exe -out ide8agent-signed.exe
    mv ide8agent-signed.exe ide8agent.exe
    cd -
fi

zip -r $archive $builddir

echo "Created archive $archive"

rm -rf $builddir
