package connect

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"runtime"
	"strconv"
	"sync"
	"time"
	_ "unsafe"

	"bitbucket.org/ide8io/ide8.io/backend/gizmo"
	"bitbucket.org/ide8io/agent/common"
	"bitbucket.org/ide8io/agent/plugin"
	"bitbucket.org/ide8io/agent/util"
	"github.com/gorilla/websocket"
)

type Connect struct {
	ConnCh    chan bool
	UpgradeCh chan string
	TunnelCh  chan TunnelEvent
	SetSessCh chan SessionEvent
	id        string
	secret    string
	username  string
	conn      *websocket.Conn
	gizmos    []gizmo.NamedGizmo
	gizmosMu  sync.Mutex
	wsmu      sync.Mutex
	plugins   []plugin.Meta
}

type TunnelEvent struct {
	ResourceId string
	Target     string
	Plugin     string
}

type SessionEvent struct {
	Name   string
	Secret string
}

type message struct {
	Type    string      `json:"type"`
	Id      string      `json:"id"`
	Payload interface{} `json:"payload"`
}

type connectPayload struct {
	Name     string             `json:"name"`
	UserName string             `json:"username"`
	Gizmos   []gizmo.NamedGizmo `json:"gizmos"`
	Plugins  []plugin.Meta      `json:"plugins"`
}

type tunnelPayload struct {
	Gizmo  string `json:"gizmo"`
	Target string `json:"target"`
	Plugin string `json:"plugin"`
}

func New(plugins []plugin.Meta, gizmos []gizmo.NamedGizmo, server string, id string, secret string, username string, gitver string) *Connect {
	c := &Connect{
		ConnCh:    make(chan bool),
		UpgradeCh: make(chan string),
		TunnelCh:  make(chan TunnelEvent),
		SetSessCh: make(chan SessionEvent),
		gizmos:    gizmos,
		plugins:   plugins,
	}
	c.id = id
	c.secret = secret
	c.username = username

	go c.run(server, gitver)
	return c
}

func (c *Connect) SetUser(username string) {
	c.gizmosMu.Lock()
	defer c.gizmosMu.Unlock()
	c.username = username
	c.sendConnect()
}

func (c *Connect) AddGizmo(gizmo gizmo.NamedGizmo) {
	c.gizmosMu.Lock()
	defer c.gizmosMu.Unlock()
	found := false
	for i := range c.gizmos {
		if c.gizmos[i].ID == gizmo.ID {
			c.gizmos[i] = gizmo
			found = true
			break
		}
	}
	if !found {
		c.gizmos = append(c.gizmos, gizmo)
	}
	c.sendMessage("add", gizmo)
}

func (c *Connect) RemoveGizmo(id string) error {
	c.gizmosMu.Lock()
	defer c.gizmosMu.Unlock()
	for i := range c.gizmos {
		if c.gizmos[i].ID == id {
			g := c.gizmos[i]
			c.gizmos = append(c.gizmos[:i], c.gizmos[i+1:]...)
			c.sendMessage("remove", g)
			return nil
		}
	}
	return fmt.Errorf("unknown resource id %s", id)
}

// Expects c.ressmu to be previously locked
func (c *Connect) sendConnect() error {
	return c.sendMessage("connect", connectPayload{
		Name:     c.id,
		UserName: c.username,
		Gizmos:   c.gizmos,
		Plugins:  c.plugins,
	})
}

func (c *Connect) sendMessage(msgType string, payload interface{}) error {
	msg := message{Type: msgType, Payload: payload}
	b, err := json.Marshal(msg)
	if err != nil {
		return err
	}
	return c.writeMessage(b)
}

func (c *Connect) writeMessage(b []byte) error {
	// websocket WriteMessage needs concurrency protection
	c.wsmu.Lock()
	defer c.wsmu.Unlock()
	if c.conn == nil {
		return nil
	}
	c.conn.SetWriteDeadline(time.Now().Add(common.WriteWait))
	return c.conn.WriteMessage(websocket.TextMessage, b)
}

//go:linkname goarm runtime.goarm
var goarm uint8

func (c *Connect) run(server string, gitver string) {
	for {
		log.Printf("connect: connecting to %s ...", server)
		startTime := time.Now()

		arch := runtime.GOARCH
		if runtime.GOARCH == "arm" {
			arch += "v" + strconv.Itoa(int(goarm))
		}
		hdr := http.Header{
			"ide8agent-version":  []string{gitver},
			"ide8agent-os":       []string{runtime.GOOS + "-" + arch},
			"ide8agent-name":     []string{c.id},
			"ide8agent-secret":   []string{c.secret},
			"ide8agent-username": []string{c.username},
		}
		conn, response, err := websocket.DefaultDialer.Dial(server, hdr)
		if err != nil {
			if response != nil {
				if response.StatusCode == http.StatusUpgradeRequired {
					body, _ := ioutil.ReadAll(response.Body)
					response.Body.Close()
					c.UpgradeCh <- string(body)
				} else if response.StatusCode == http.StatusUnauthorized {
					body, _ := ioutil.ReadAll(response.Body)
					response.Body.Close()
					log.Fatalf("Authorization failed: %s", string(body))
				}
			}
			util.RelaxReconnect(context.Background(), startTime, 10*time.Second)
			continue
		}
		c.wsmu.Lock()
		c.conn = conn
		c.wsmu.Unlock()

		log.Println("connect: connected")

		name := response.Header.Get("ide8agent-name")
		secret := response.Header.Get("ide8agent-secret")
		if name != "" && secret != "" && (name != c.id || secret != c.secret) {
			c.id = name
			c.secret = secret
			c.SetSessCh <- SessionEvent{name, secret}
		}

		c.gizmosMu.Lock()
		ret := c.sendConnect()
		c.gizmosMu.Unlock()
		if ret != nil {
			log.Println("connect: sendConnect:", ret)
			c.conn.Close()
			c.wsmu.Lock()
			c.conn = nil
			c.wsmu.Unlock()
			continue
		}

		c.ConnCh <- true

		killping := make(chan struct{})
		go func() {
			for {
				select {
				case <-time.After(common.PongPeriod):
					err := conn.WriteControl(websocket.PingMessage, []byte{}, time.Now().Add(common.WriteWait))
					if err != nil {
						log.Println("connect: ping write:", err)
					}
				case <-killping:
					return
				}
			}
		}()

		conn.SetReadDeadline(time.Now().Add(common.PongWait))
		conn.SetPongHandler(func(string) error {
			conn.SetReadDeadline(time.Now().Add(common.PongWait))
			return nil
		})

		for {
			_, b, err := conn.ReadMessage()
			if err != nil {
				log.Println("connect:", err)
				break
			}
			log.Printf("connect: read: %s", b)

			var payload json.RawMessage
			msg := message{Payload: &payload}
			err = json.Unmarshal(b, &msg)
			if err != nil {
				log.Println(err)
			}

			if msg.Type == "tunnel" {
				var msg tunnelPayload
				err := json.Unmarshal(payload, &msg)
				if err != nil {
					log.Println("connect: tunnel:", err)
					return
				}
				c.TunnelCh <- TunnelEvent{msg.Gizmo, msg.Target, msg.Plugin}
			} else {
				log.Println("connect: Unknown message type", msg.Type)
			}
		}
		killping <- struct{}{}
		log.Println("connect: disconnected")
		conn.Close()
		c.wsmu.Lock()
		c.conn = nil
		c.wsmu.Unlock()
		c.ConnCh <- false
	}
}
