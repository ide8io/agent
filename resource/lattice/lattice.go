package lattice

import (
	"fmt"
	"log"

	"bitbucket.org/ide8io/ide8.io/backend/gizmo"
	"bitbucket.org/ide8io/agent/plugin"
	"bitbucket.org/ide8io/agent/resource"
	"bitbucket.org/ide8io/agent/tunnel"
	"bitbucket.org/ide8io/agent/tunnel/plugin"
	"github.com/google/gousb"
)

type latticeResource struct {
	gizmo  gizmo.NamedGizmo
	tunnel tunnel.Tunnel
}

func (r *latticeResource) Gizmo() gizmo.NamedGizmo {
	return r.gizmo
}

func (r *latticeResource) SetTunnel(createplugin plugin.CreatePluginFunc, target string) {
	if r.tunnel == nil {
		p, err := createplugin("iCEburn")
		if err != nil {
			log.Println("settunnel:", err)
			return
		}
		r.tunnel = tunnel_plugin.New(p, target)
	} else {
		r.tunnel.SetTarget(target)
	}
}

func (r *latticeResource) CloseTunnel() {
	if r.tunnel != nil {
		r.tunnel.Close()
		r.tunnel = nil
	}
}

func Probe(dev *gousb.Device) resource.Resource {
	ser, err := dev.SerialNumber()
	if err != nil {
		return nil
	}
	vid := fmt.Sprintf("%04x", dev.Desc.Vendor)
	pid := fmt.Sprintf("%04x", dev.Desc.Product)
	id := fmt.Sprintf("%04x_%04x_%s_0", dev.Desc.Vendor, dev.Desc.Product, ser)
	serial, _ := dev.SerialNumber()
	return &latticeResource{
		gizmo: gizmo.NamedGizmo{
			ID: id,
			Gizmo: gizmo.Gizmo{
				Class:  "interface",
				If:     "0",
				Serial: serial,
				VID:    vid,
				PID:    pid,
			},
		},
	}
}
