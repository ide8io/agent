package resource

import (
	"bitbucket.org/ide8io/ide8.io/backend/gizmo"
	"bitbucket.org/ide8io/agent/plugin"
)

type Resource interface {
	Gizmo() gizmo.NamedGizmo
	SetTunnel(createplugin plugin.CreatePluginFunc, target string)
	CloseTunnel()
}
