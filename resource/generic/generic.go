package generic

import (
	"strings"

	"bitbucket.org/ide8io/go-serial/enumerator"
	"bitbucket.org/ide8io/ide8.io/backend/gizmo"
	"bitbucket.org/ide8io/agent/plugin"
	"bitbucket.org/ide8io/agent/resource"
	"bitbucket.org/ide8io/agent/tunnel"
	"bitbucket.org/ide8io/agent/tunnel/serial"
)

type genericResource struct {
	gizmo  gizmo.NamedGizmo
	tunnel tunnel.Tunnel
}

func (r *genericResource) Gizmo() gizmo.NamedGizmo {
	return r.gizmo
}

func (r *genericResource) SetTunnel(createplugin plugin.CreatePluginFunc, target string) {
	if r.tunnel == nil {
		r.tunnel = serial.OpenSerialTunnel(r.gizmo.Dev, 9600, target)
	} else {
		r.tunnel.SetTarget(target)
	}
}

func (r *genericResource) CloseTunnel() {
	if r.tunnel != nil {
		r.tunnel.Close()
		r.tunnel = nil
	}
}

func Probe(pd *enumerator.PortDetails) resource.Resource {
	id := ""
	if pd.IsUSB {
		id += pd.VID + "_" + pd.PID + "_" + pd.SerialNumber + "_0"
	} else {
		id += strings.TrimLeft(strings.Replace(pd.Name, "/", "_", -1), "_")
	}
	return &genericResource{
		gizmo: gizmo.NamedGizmo{
			ID: id,
			Gizmo: gizmo.Gizmo{
				Class:  "serial",
				If:     "0",
				Serial: pd.SerialNumber,
				Dev:    pd.Name,
				VID:    pd.VID,
				PID:    pd.PID,
			},
		},
	}
}
