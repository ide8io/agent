package jlink

import (
	"log"
	"strings"

	"bitbucket.org/ide8io/go-serial/enumerator"
	"bitbucket.org/ide8io/ide8.io/backend/gizmo"
	"bitbucket.org/ide8io/agent/plugin"
	"bitbucket.org/ide8io/agent/resource"
	"bitbucket.org/ide8io/agent/tunnel"
	"bitbucket.org/ide8io/agent/tunnel/plugin"
	"bitbucket.org/ide8io/agent/tunnel/serial"
)

type jlinkResource struct {
	gizmo  gizmo.NamedGizmo
	tunnel tunnel.Tunnel
}

func (r *jlinkResource) Gizmo() gizmo.NamedGizmo {
	return r.gizmo
}

func (r *jlinkResource) SetTunnel(createplugin plugin.CreatePluginFunc, target string) {
	if r.tunnel == nil {
		p, err := createplugin("openocd")
		if err != nil {
			log.Println("settunnel:", err)
			return
		}
		r.tunnel = tunnel_plugin.New(p, target)
	} else {
		r.tunnel.SetTarget(target)
	}
}

func (r *jlinkResource) CloseTunnel() {
	if r.tunnel != nil {
		r.tunnel.Close()
		r.tunnel = nil
	}
}

type serialResource struct {
	gizmo  gizmo.NamedGizmo
	tunnel tunnel.Tunnel
}

func (r *serialResource) Gizmo() gizmo.NamedGizmo {
	return r.gizmo
}

func (r *serialResource) SetTunnel(createplugin plugin.CreatePluginFunc, target string) {
	if r.tunnel == nil {
		r.tunnel = serial.OpenSerialTunnel(r.gizmo.Dev, 9600, target)
	} else {
		r.tunnel.SetTarget(target)
	}
}

func (r *serialResource) CloseTunnel() {
	if r.tunnel != nil {
		r.tunnel.Close()
		r.tunnel = nil
	}
}

func Probe(pd *enumerator.PortDetails) []resource.Resource {
	id := pd.VID + "_" + pd.PID + "_" + pd.SerialNumber
	if strings.ToUpper(pd.VID) == "1366" && strings.ToUpper(pd.PID) == "1015" {
		return []resource.Resource{
			&serialResource{
				gizmo: gizmo.NamedGizmo{
					ID: id + "_0",
					Gizmo: gizmo.Gizmo{
						Class:  "serial",
						If:     "0",
						Serial: pd.SerialNumber,
						Dev:    pd.Name,
						VID:    pd.VID,
						PID:    pd.PID,
					},
				},
			},
			&jlinkResource{
				gizmo: gizmo.NamedGizmo{
					ID: id + "_2",
					Gizmo: gizmo.Gizmo{
						Class:  "vendor",
						If:     "2",
						Serial: pd.SerialNumber,
						VID:    pd.VID,
						PID:    pd.PID,
					},
				},
			},
		}
	}
	return nil
}
