package scanner

import (
	"log"
	"sync"
	"time"

	"bitbucket.org/ide8io/go-serial/enumerator"
	"bitbucket.org/ide8io/agent/resource"
	"bitbucket.org/ide8io/agent/resource/arduino"
	"bitbucket.org/ide8io/agent/resource/generic"
	"bitbucket.org/ide8io/agent/resource/jlink"
	"bitbucket.org/ide8io/agent/resource/micropython"
	"github.com/google/gousb"
)

var AddCh = make(chan resource.Resource)
var RemCh = make(chan resource.Resource)
var ports []*enumerator.PortDetails
var resMap = map[string][]resource.Resource{}
var resMapMu sync.Mutex
var latt bool

func Init(resTypeMapping map[string]string) []resource.Resource {
	// Do initial scan and return
	usbctx := gousb.NewContext()
	addRes, _ := scan(usbctx, resTypeMapping)
	go func() {
		defer usbctx.Close()
		for {
			time.Sleep(time.Second)
			addRes, remRes := scan(usbctx, resTypeMapping)
			for _, r := range addRes {
				AddCh <- r
			}
			for _, r := range remRes {
				RemCh <- r
			}
		}
	}()
	return addRes
}

func scan(usbctx *gousb.Context, resTypeMapping map[string]string) (addRes []resource.Resource, remRes []resource.Resource) {
	/*	dev, err := usbctx.OpenDeviceWithVIDPID(0x1443, 7)
		if err != nil {
			log.Println("resource.scan:", err)
		} else {
			resMapMu.Lock()
			if dev != nil {
				defer dev.Close()
				if !latt {
					latt = true
					res := lattice.Probe(dev)
					resMap["lattice"] = []resource.Resource{res}
					addRes = append(addRes, res)
				}
			} else {
				if latt {
					latt = false
					res, ok := resMap["lattice"]
					if !ok {
						log.Println("resource.scan: Can't find resource to delete: lattice")
					} else {
						delete(resMap, "lattice")
						for _, r := range res {
							r.CloseTunnel()
						}
						remRes = append(remRes, res...)
					}
				}
			}
			resMapMu.Unlock()
		}
	*/
	foundports, err := enumerator.GetDetailedPortsList()
	if err != nil {
		log.Println("resource.scan:", err)
		return
	}

	resMapMu.Lock()
	defer resMapMu.Unlock()

	addPorts := []*enumerator.PortDetails{}
loop:
	for _, pn := range foundports {
		for i, po := range ports {
			if *pn == *po {
				ports = append(ports[:i], ports[i+1:]...)
				continue loop
			}
		}
		addPorts = append(addPorts, pn)
	}

	for _, p := range addPorts {
		resType := resTypeMapping[p.Name]
		res := probe(p, resType)
		if res == nil {
			log.Println("Unknown serial resource:", p)
			continue
		}
		resMap[p.Name] = res
		addRes = append(addRes, res...)
	}

	remPorts := ports
	ports = foundports

	for _, p := range remPorts {
		res, ok := resMap[p.Name]
		if !ok {
			log.Println("resource.scan: Can't find resource to delete:", p)
			continue
		}
		delete(resMap, p.Name)
		for _, r := range res {
			r.CloseTunnel()
		}
		remRes = append(remRes, res...)
	}
	return
}

func probe(pd *enumerator.PortDetails, resType string) []resource.Resource {
	if res := arduino.Probe(pd, resType); res != nil {
		return []resource.Resource{res}
	}
	if res := jlink.Probe(pd); len(res) > 0 {
		return res
	}
	if res := micropython.Probe(pd, resType); res != nil {
		return []resource.Resource{res}
	}
	if res := generic.Probe(pd); res != nil {
		return []resource.Resource{res}
	}
	return nil
}

func GetResource(id string) resource.Resource {
	resMapMu.Lock()
	defer resMapMu.Unlock()
	for _, resList := range resMap {
		for _, r := range resList {
			if r.Gizmo().ID == id {
				return r
			}
		}
	}
	return nil
}
