package arduino

import (
	"log"
	"regexp"
	"strings"

	"bitbucket.org/ide8io/go-serial/enumerator"
	"bitbucket.org/ide8io/ide8.io/backend/gizmo"
	"bitbucket.org/ide8io/agent/plugin"
	"bitbucket.org/ide8io/agent/resource"
	"bitbucket.org/ide8io/agent/tunnel"
	"bitbucket.org/ide8io/agent/tunnel/serial"
)

type arduinoResource struct {
	gizmo  gizmo.NamedGizmo
	tunnel tunnel.Tunnel
}

func (r *arduinoResource) Gizmo() gizmo.NamedGizmo {
	return r.gizmo
}

func (r *arduinoResource) SetTunnel(createplugin plugin.CreatePluginFunc, target string) {
	if r.tunnel == nil {
		r.tunnel = serial.OpenSerialTunnel(r.gizmo.Dev, 9600, target)
	} else {
		r.tunnel.SetTarget(target)

	}
}

func (r *arduinoResource) CloseTunnel() {
	if r.tunnel != nil {
		r.tunnel.Close()
		r.tunnel = nil
	}
}

func isUNO(vid string, pid string) bool {
	vid = strings.ToUpper(vid)
	return (vid == "2341" && pid == "0043") || (vid == "2341" && pid == "0001") || (vid == "2A03" && pid == "0043") || (vid == "2341" && pid == "0243")
}

func isArduinoUNOString(name string) bool {
	matched, err := regexp.MatchString("(?i)^arduino.?uno$", name)
	if err != nil {
		log.Println("regex error:", err)
		return false
	}
	return matched
}

func Probe(pd *enumerator.PortDetails, resType string) resource.Resource {
	id := pd.VID + "_" + pd.PID + "_" + pd.SerialNumber + "_0"
	if isArduinoUNOString(resType) || isUNO(pd.VID, pd.PID) {
		return &arduinoResource{
			gizmo: gizmo.NamedGizmo{
				ID: id,
				Gizmo: gizmo.Gizmo{
					Class:  "serial",
					If:     "0",
					Dev:    pd.Name,
					Serial: pd.SerialNumber,
					VID:    pd.VID,
					PID:    pd.PID,
				},
			},
		}
	}
	return nil
}
