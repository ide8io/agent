package plugin

import (
	"context"

	"github.com/golang/protobuf/ptypes/any"
)

type CreatePluginFunc func(id string) (Plugin, error)

type Plugin interface {
	Close()
	Online()
	Send(ctx context.Context, any *any.Any) error
	Recv(ctx context.Context) (*any.Any, error)
}
