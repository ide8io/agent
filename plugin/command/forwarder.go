package command

import (
	"net"
	"strconv"
)

type Forwarder struct {
	conn net.Conn
}

func NewForwarder(port uint16) (*Forwarder, error) {
	conn, err := net.Dial("tcp", "localhost:"+strconv.Itoa(int(port)))
	if err != nil {
		return nil, err
	}
	return &Forwarder{
		conn: conn,
	}, nil
}

func (f *Forwarder) Send(data []byte) error {
	_, err := f.conn.Write(data)
	return err
}

func (f *Forwarder) Recv() ([]byte, error) {
	data := make([]byte, 1024)
	n, err := f.conn.Read(data)
	if err != nil {
		return nil, err
	}
	return data[:n], nil
}

func (f *Forwarder) Close() {
	f.conn.Close()
}
