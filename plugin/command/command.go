package command

import (
	"context"
	"io"
	"log"
	"os/exec"
	"sync"
	"time"

	"bitbucket.org/ide8io/ide8.io/backend/api/tunnel/remote"
	"bitbucket.org/ide8io/agent/util"
	"github.com/golang/protobuf/proto"
	"github.com/golang/protobuf/ptypes/any"
)

type Plugin struct {
	binary      string
	args        []string
	mu          sync.Mutex
	status      api_tunnel_remote.Status
	statusCh    chan api_tunnel_remote.Status
	outputCh    chan []byte
	forwardCh   chan api_tunnel_remote.ForwardData
	fwdStatusCh chan api_tunnel_remote.ForwardStatus_Forward
	cmd         *exec.Cmd
	fwdMap      map[int32]*Forwarder
}

func New(binary string, args []string) *Plugin {
	return &Plugin{
		binary:      binary,
		args:        args,
		statusCh:    make(chan api_tunnel_remote.Status),
		outputCh:    make(chan []byte),
		forwardCh:   make(chan api_tunnel_remote.ForwardData),
		fwdStatusCh: make(chan api_tunnel_remote.ForwardStatus_Forward),
		fwdMap:      make(map[int32]*Forwarder),
	}
}

func (p *Plugin) Close() {
	go func() {
		// Flush out receive chans to close cleanly
		for {
			_, err := p.Recv(context.Background())
			if err != nil {
				break
			}
		}
	}()
	p.mu.Lock()
	for _, fwd := range p.fwdMap {
		fwd.Close()
	}
	cmd := p.cmd
	p.mu.Unlock()
	if cmd != nil {
		cmd.Process.Kill()
	}
	// A sleep to allow return status to be sent to outputCh before it's closed.
	// Needs cleaner implementation...
	time.Sleep(time.Second)
	close(p.outputCh)
}

func (p *Plugin) Online() {
	p.statusCh <- p.status
}

func (p *Plugin) Send(ctx context.Context, msg *any.Any) error {
	switch msg.TypeUrl {
	case proto.MessageName(&api_tunnel_remote.Start{}):
		var start api_tunnel_remote.Start
		err := proto.Unmarshal(msg.Value, &start)
		if err != nil {
			log.Println("openocd plugin: start:", err)
			break
		}
		p.start(start)
	case proto.MessageName(&api_tunnel_remote.Stop{}):
		p.mu.Lock()
		defer p.mu.Unlock()
		if p.cmd != nil {
			err := p.cmd.Process.Kill()
			if err != nil {
				log.Println("openocd kill:", err)
			}
		}
	case proto.MessageName(&api_tunnel_remote.ForwardOpen{}):
		var open api_tunnel_remote.ForwardOpen
		err := proto.Unmarshal(msg.Value, &open)
		if err != nil {
			log.Println("openocd plugin: fwd open:", err)
			break
		}
		p.mu.Lock()
		defer p.mu.Unlock()
		fwd, ok := p.fwdMap[open.Port]
		if ok {
			log.Println("openocd plugin: port", open.Port, "already open")
			p.fwdStatusCh <- api_tunnel_remote.ForwardStatus_Forward{Port: open.Port, Open: true}
			break
		}
		log.Println("openocd plugin: opening port", open.Port)
		fwd, err = NewForwarder(uint16(open.Port))
		if err != nil {
			log.Println("openocd plugin: opening port failed:", err)
			p.fwdStatusCh <- api_tunnel_remote.ForwardStatus_Forward{Port: open.Port, Open: false, Error: err.Error()}
			break
		}
		log.Println("openocd plugin: opening port successful")
		p.fwdStatusCh <- api_tunnel_remote.ForwardStatus_Forward{Port: open.Port, Open: true}
		p.fwdMap[open.Port] = fwd
		go func() {
			var err error
			for {
				var data []byte
				data, err = fwd.Recv()
				if err != nil {
					break
				}
				p.forwardCh <- api_tunnel_remote.ForwardData{Port: open.Port, Data: data}
			}
			p.fwdStatusCh <- api_tunnel_remote.ForwardStatus_Forward{Port: open.Port, Open: false, Error: err.Error()}
			p.mu.Lock()
			defer p.mu.Unlock()
			delete(p.fwdMap, open.Port)
		}()
	case proto.MessageName(&api_tunnel_remote.ForwardClose{}):
		var open api_tunnel_remote.ForwardOpen
		err := proto.Unmarshal(msg.Value, &open)
		if err != nil {
			log.Println("openocd plugin: fwd close:", err)
			break
		}
		p.mu.Lock()
		defer p.mu.Unlock()
		fwd, ok := p.fwdMap[open.Port]
		if !ok {
			log.Println("openocd plugin: no forwarder for port", open.Port)
			break
		}
		fwd.Close()
	case proto.MessageName(&api_tunnel_remote.ForwardData{}):
		var open api_tunnel_remote.ForwardData
		err := proto.Unmarshal(msg.Value, &open)
		if err != nil {
			log.Println("openocd plugin: fwd data:", err)
			break
		}
		p.mu.Lock()
		defer p.mu.Unlock()
		fwd, ok := p.fwdMap[open.Port]
		if !ok {
			log.Println("openocd plugin: no forwarder for port", open.Port)
			break
		}
		err = fwd.Send(open.Data)
		if err != nil {
			fwd.Close()
		}
	default:
		log.Println("openocd plugin: unknown message type:", msg.TypeUrl)
	}
	return nil
}

func (p *Plugin) Recv(ctx context.Context) (*any.Any, error) {
	var pb proto.Message
	select {
	case status := <-p.statusCh:
		pb = &status
	case data, ok := <-p.outputCh:
		if !ok {
			return nil, io.EOF
		}
		pb = &api_tunnel_remote.Data{
			Type:    "stdout",
			Payload: data,
		}
	case fwdstatus, ok := <-p.fwdStatusCh:
		if !ok {
			return nil, io.EOF
		}
		pb = &api_tunnel_remote.ForwardStatus{
			ForwardList: []*api_tunnel_remote.ForwardStatus_Forward{
				&fwdstatus,
			},
		}
	case fwddata, ok := <-p.forwardCh:
		if !ok {
			return nil, io.EOF
		}
		pb = &fwddata
	case <-ctx.Done():
		return nil, ctx.Err()
	}
	b, err := proto.Marshal(pb)
	if err != nil {
		return nil, err
	}
	return &any.Any{TypeUrl: proto.MessageName(pb), Value: b}, nil
}

func (p *Plugin) start(start api_tunnel_remote.Start) {
	setStatus := func(running bool, errmsg string) {
		if errmsg != "" {
			log.Println("openocd plugin: startcmd:", errmsg)
		}
		p.status.Running = running
		p.status.Error = errmsg
		p.statusCh <- p.status
	}
	p.mu.Lock()
	defer p.mu.Unlock()
	if p.cmd != nil {
		setStatus(true, "already running")
		return
	}
	args, err := util.ParseCommandLine(start.Arguments)
	if err != nil {
		setStatus(false, err.Error())
		return
	}
	cmd := exec.Command(p.binary, append([]string{}, args...)...)
	stdout, err := cmd.StdoutPipe()
	if err != nil {
		setStatus(false, err.Error())
		return
	}
	stderr, err := cmd.StderrPipe()
	if err != nil {
		setStatus(false, err.Error())
		return
	}
	err = cmd.Start()
	if err != nil {
		p.outputCh <- []byte("\n" + err.Error() + "\n")
		setStatus(false, err.Error())
		return
	}
	setStatus(true, "")
	p.cmd = cmd
	go func() {
		for {
			buf := make([]byte, 1024)
			n, err := stdout.Read(buf)
			if n > 0 {
				p.outputCh <- buf[:n]
			}
			if err == io.EOF {
				break
			}
			if err != nil {
				log.Print("openocd plugin stdout read:", err)
			}
		}
		log.Println("waiting for command to end")
		err := cmd.Wait()
		if err != nil {
			exiterr, ok := err.(*exec.ExitError)
			if ok {
				log.Println("command exitited with error:", exiterr)
				p.outputCh <- []byte("\n" + exiterr.Error() + "\n")
			} else {
				log.Println("command wait failure:", err)
			}
		} else {
			log.Println("command exited successfully")
		}
		p.mu.Lock()
		p.cmd = nil
		p.mu.Unlock()
		setStatus(false, err.Error())
	}()
	go func() {
		for {
			buf := make([]byte, 1024)
			n, err := stderr.Read(buf)
			if n > 0 {
				p.outputCh <- buf[:n]
			}
			if err == io.EOF {
				break
			}
			if err != nil {
				log.Print("openocd plugin stderr read:", err)
			}
		}
	}()
}
