package plugin

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"path/filepath"

	"bitbucket.org/ide8io/agent/plugin/command"
	yaml "gopkg.in/yaml.v2"
)

const (
	pluginsDir = "plugins"
)

type binary struct {
	binary string
	path   string
}

type Plugins struct {
	Plugins []Meta
	mapping map[string]*binary
}

type Meta struct {
	Name         string                            `json:"name"`
	Exe          string                            `json:"-"`
	Version      string                            `json:"version"`
	Build        string                            `json:"build"`
	Capabilities map[string]map[string]interface{} `json:"capabilties"`
}

func Scan(mapping map[string]string) (*Plugins, error) {
	p := &Plugins{
		mapping: make(map[string]*binary),
	}

	pdirs := []string{pluginsDir}
	exepath, err := os.Executable()
	if err == nil {
		pdirs = append(pdirs, filepath.Join(filepath.Dir(exepath), pluginsDir))
	}
	for _, pdir := range pdirs {
		dirs, err := ioutil.ReadDir(pdir)
		if err != nil {
			continue
		}
		for _, d := range dirs {
			if !d.IsDir() {
				continue
			}
			b, err := ioutil.ReadFile(filepath.Join(pdir, d.Name(), "ide8plugin.yaml"))
			if err != nil {
				log.Println("no plugin meta for", d.Name())
				continue
			}
			var meta Meta
			err = yaml.Unmarshal(b, &meta)
			if err != nil {
				log.Println("plugin meta parse error:", err)
				continue
			}
			_, ok := p.mapping[meta.Name]
			if ok {
				log.Println("plugin already found:", meta.Name)
				continue
			}
			log.Println("plugin found:", d.Name())
			p.mapping[meta.Name] = &binary{
				binary: filepath.Base(meta.Exe),
				path:   filepath.Join(pdir, d.Name(), filepath.Dir(meta.Exe)),
			}
			p.Plugins = append(p.Plugins, meta)
		}
	}

	for k, v := range mapping {
		path, err := exec.LookPath(v)
		if err != nil {
			return nil, err
		}
		log.Println("plugin scan:", path)
		_, ok := p.mapping[k]
		if ok {
			log.Println("plugin already found:", k)
			continue
		}
		p.mapping[k] = &binary{binary: v, path: filepath.Dir(path)}
	}
	return p, nil
}

func (p *Plugins) CreatePlugin(id string) (Plugin, error) {
	plug, ok := p.mapping[id]
	if !ok {
		return nil, fmt.Errorf("no such plugin: %v", id)
	}
	args := []string{}
	if id == "openocd" {
		args = append(args, "-c", "bindto localhost")
	}
	return command.New(filepath.Join(plug.path, plug.binary), args), nil
}
