package common

import "time"

const (
	WriteWait  = time.Second * 10
	PongPeriod = time.Minute
	PongWait   = PongPeriod + time.Second*10
)
