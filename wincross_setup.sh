LIBUSB_VERSION=1.0.22

if [ ! -d "libusb-$LIBUSB_VERSION" ]; then
  mkdir libusb-$LIBUSB_VERSION
  cd libusb-$LIBUSB_VERSION
  curl -L https://github.com/libusb/libusb/releases/download/v$LIBUSB_VERSION/libusb-$LIBUSB_VERSION.7z -o libusb-$LIBUSB_VERSION.7z
  7z x libusb-$LIBUSB_VERSION.7z
  cat <<EOT > libusb-1.0.pc
Name: libusb-1.0
Description: C API for USB device access from Linux, Mac OS X, Windows, OpenBSD/NetBSD and Solaris userspace
Version: $LIBUSB_VERSION
Libs: -L$PWD/MinGW32/static -lusb-1.0
Libs.private: -ludev  -pthread
Cflags: -I$PWD/include/libusb-1.0
EOT
  cd -
fi

export PKG_CONFIG_LIBDIR=$PWD/libusb-$LIBUSB_VERSION
export CC=i686-w64-mingw32-gcc
export CGO_ENABLED=1
export GOOS=windows
export GOARCH=386
