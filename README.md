# IDE8 agent #

## Prerequsites ##

Go version 1.8 or newer

Clone repository (Replace \<user> with your username):

`git clone <user>@bitbucket.org:ide8io/agent $GOPATH/src/bitbucket.org/ide8io/agent`

Also clone other private repos (Since go get can't do it):

`git clone <user>@bitbucket.org:ide8io/go-serial $GOPATH/src/bitbucket.org/ide8io/go-serial`

On Linux install the the following for systray support:

`apt install libgtk-3-dev libappindicator3-dev`

## Building ##

Fetch dependencies:
`go get`

Build:
`go build`

Cross build to arm:
`GOOS=linux GOARCH=arm go build`

Cross build to windows:
`CGO_LDFLAGS=-L${PWD}/headless/libusb/MinGW32/static CC=i686-w64-mingw32-gcc CXX=i686-w64-mingw32-g++ GOOS=windows GOARCH=386 CGO_ENABLED=1 go build`
## Running ##

`go run *.go`

Use `-help` for available options.

For desktop use; a systray icon appears where you can assing to user.

User assignment can also be enforced with the `-user` option or made public with the `-public` option.
